package sync2

import (
	"sync"
	"testing"
	"time"
)

func TestLock(t *testing.T) {
	tt := NewThrottler(2)
	st := time.Now()

	wg := sync.WaitGroup{}
	wg.Add(10)

	for i := 0; i < 10; i++ {
		go func(i int) {
			defer tt.Lock().Unlock()
			time.Sleep(10 * time.Millisecond)
			wg.Done()
		}(i)
	}

	wg.Wait()
	duration := time.Now().Sub(st).Nanoseconds() / 1000000
	if duration < 50 || duration > 52 {
		t.Fatal("Throtte fails, duration:", duration)
	}
}
