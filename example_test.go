package sync2

import (
	"fmt"
	"sync"
	"time"
)

func Example() {
	t := NewThrottler(2)

	//demo code
	st := time.Now()
	wg := &sync.WaitGroup{}
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(i int) {
			fmt.Print(">")
			//used to throttle
			defer t.Lock().Unlock()
			defer wg.Done()
			fmt.Print(i, "W")
			time.Sleep(100 * time.Millisecond) //to simulate work
			fmt.Print("<")
		}(i)
	}

	wg.Wait()                                            //let the goroutines finish
	fmt.Println("\nTime to finish:", time.Now().Sub(st)) //should be 100ms * 10/2

	//Output
	//>0W>1W>>>>>>>><<2W3W<<4W5W<<6W7W<<8W9W<<
	//Time to finish: 501.110852ms
}
